import mongoose from 'mongoose';

const logSchema = mongoose.Schema(
    {

        appId:{
            type: String,
            require: [true,"Please add a Tenant ID"]
        },
        tenantId:{
            type: String,
            require: [true,"Please add a Tenant ID"]
        },
        companyId:{
            type: String,
            require: [true,"Please add a Tenant ID"]
        },
        branchId:{
            type: String,
            require: [true,"Please add a Tenant ID"]
        },
        moduleName:{
            type: String,
            require: [true,"Please add a module Name "]
        },
        resourceName:{
            type: String,
            require: [true,"Please add a Resource Name"]
        },
        eventName:{
            type: String,
            require: [true,"Please add a Event Name"]
        },
        logContentEn:{
            type: String,
            require: [true,"Please add a Log content"]
        },
        logContentAr:{
            type: String,
            require: [true,"Please add a Log content"]
        },
        userId:{
            type: String,
            require: [true,"Please add a User ID"]
        }, 
        userName:{
            type: String,
            require: [true,"Please add a User Name"]
        }, 
    },
    {
        timestamps: true,
    });


const logModel =  mongoose.model('Log',logSchema);

export default logModel;