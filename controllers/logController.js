import Log from '../models/logModel.js';

const addLog = async (req,res) => {
    const {
        appId,
        tenantId,
        companyId,
        branchId,
        moduleName,
        resourceName,
        eventName,
        logContentEn,
        logContentAr,
        userId,
        userName
    } = req.body;

    if(!tenantId){
        res.status(400);
        throw new Error("Tenant id is required");
    }

    const added = await Log.create({
        appId,
        tenantId,
        companyId,
        branchId,
        moduleName,
        resourceName,
        eventName,
        logContentEn,
        logContentAr,
        userId,
        userName
    });

    if(added){
        res.status(201).json({status:1 ,message:"Log has been added successffuly",data:added});
    }else{
        res.status(404).json({"status":0,"message":"Error during add Logs"});
    }
}

const getLog = async (req, res) =>{
    const tenantId = req.params.tenantId;
    const logs = await Log.find({
        tenantId
    }).sort({createdAt:-1}).limit(50);
    if(logs){
        res.status(200).json(logs);
    }else{
        res.status(404).json({"status":0,"message":"No data found"});
    }
}

export {addLog,getLog}