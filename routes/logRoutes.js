import express from "express";

import {addLog,getLog} from '../controllers/logController.js';

const router = express.Router();

router.post('/',addLog);
router.get('/:tenantId',getLog);

export default router;