import  express from 'express';
import dotenv from 'dotenv' ;
import  cors from 'cors';

dotenv.config();
import dbConnect from './config/dbConnection.js';

// Start routes includes
import logRoutes from './routes/logRoutes.js';

dbConnect();
const app = express();
const port = process.env.PORT || 5553;

app.use(cors({ origin: '*',}));
app.use(express.json());

// Start routes
app.use('/logs',logRoutes);


app.listen(port,()=> {
    console.log(`Server listening on port ${port}`);
});